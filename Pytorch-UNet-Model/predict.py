import argparse
import logging
import os
import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image
from torchvision import transforms
import glob
import multiprocessing as mp
from utils.data_loading import BasicDataset
from unet import UNet
from utils.utils import plot_img_and_mask


def predict_img(net, full_img, device, scale_factor=1, out_threshold=0.5):
    net.eval()
    img = torch.from_numpy(BasicDataset.preprocess(None, full_img, scale_factor, is_mask=False))
    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        output = net(img).cpu()
        output = F.interpolate(output, (full_img.size[1], full_img.size[0]), mode='bilinear')
        if net.n_classes > 1:
            mask = output.argmax(dim=1)
        else:
            mask = torch.sigmoid(output) > out_threshold

    return mask[0].long().squeeze().numpy()


def get_output_filenames(args):
    def _generate_name(fn):
        return f'{os.path.splitext(fn)[0]}_OUT.png'
    return args.output or list(map(_generate_name, args.input))


def mask_to_image(mask: np.ndarray, mask_values):
    if isinstance(mask_values[0], list):
        out = np.zeros((mask.shape[-2], mask.shape[-1], len(mask_values[0])), dtype=np.uint8)
    elif mask_values == [0, 1]:
        out = np.zeros((mask.shape[-2], mask.shape[-1]), dtype=bool)
    else:
        out = np.zeros((mask.shape[-2], mask.shape[-1]), dtype=np.uint8)

    if mask.ndim == 3:
        mask = np.argmax(mask, axis=0)

    for i, v in enumerate(mask_values):
        out[mask == i] = v

    return Image.fromarray(out)


def get_args():
    parser = argparse.ArgumentParser(description='Predict masks from input images')
    parser.add_argument('--model', '-m', default='MODEL.pth', metavar='FILE',
                        help='Specify the file in which the model is stored')
    parser.add_argument('--input', '-i', metavar='INPUT', help='Directory containing input images', required=True)
    parser.add_argument('--output', '-o', metavar='OUTPUT', help='Directory for output mask images', required=True)
    parser.add_argument('--viz', '-v', action='store_true',
                        help='Visualize the images as they are processed')
    parser.add_argument('--no-save', '-n', action='store_true', help='Do not save the output masks')
    parser.add_argument('--mask-threshold', '-t', type=float, default=0.5,
                        help='Minimum probability value to consider a mask pixel white')
    parser.add_argument('--scale', '-s', type=float, default=1,
                        help='Scale factor for the input images')
    parser.add_argument('--bilinear', action='store_true', default=False, help='Use bilinear upsampling')
    parser.add_argument('--classes', '-c', type=int, default=2, help='Number of classes')
    parser.add_argument('--patch-size', type=int, default=256, help='Size of the patches for large images')
    parser.add_argument('--input-type','-it', choices=['patches', 'large'], default='patches', help='Input type: patches or large images')

    return parser.parse_args()


def predict_patch(net, img_patch, device, scale_factor, out_threshold, x, y, patch_size):
    mask_patch = predict_img(net, img_patch, device, scale_factor, out_threshold)
    return (mask_patch, x, y)

def predict_large_image(net, img, device, patch_size, scale_factor, out_threshold):
    img = np.array(img)
    height, width, _ = img.shape
    mask = np.zeros((height, width), dtype=np.uint8)

    patches = []
    for y in range(0, height, patch_size):
        for x in range(0, width, patch_size):
            img_patch = img[y:y + patch_size, x:x + patch_size]
            img_patch = Image.fromarray(img_patch)
            patches.append((img_patch, x, y))

    # Process patches in parallel
    with mp.Pool(processes=mp.cpu_count()) as pool:
        results = pool.starmap(predict_patch, [(net, patch[0], device, scale_factor, out_threshold, patch[1], patch[2], patch_size) for patch in patches])

    # Combine the patches to form the final mask
    for result in results:
        mask_patch, x, y = result
        mask[y:y + patch_size, x:x + patch_size] = mask_patch[:patch_size, :patch_size]

    return mask

if __name__ == '__main__':
    args = get_args()
    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

    input_dir = args.input
    output_dir = args.output

    # Load the model
    net = UNet(n_channels=3, n_classes=args.classes, bilinear=args.bilinear)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    net.to(device=device)
    state_dict = torch.load(args.model, map_location=device)
    net.load_state_dict(state_dict, strict=False)
    net.eval()

    # Get the input files
    input_files = glob.glob(os.path.join(input_dir, '*.jpg')) + glob.glob(os.path.join(input_dir, '*.png'))
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for input_file in input_files:
        logging.info(f'Processing image: {input_file}')
        img = Image.open(input_file)

        if args.input_type == 'patches':
            # If input is patches, predict the mask directly
            mask = predict_img(net, img, device, scale_factor=args.scale, out_threshold=args.mask_threshold)
        else:
            # If input is large image, predict mask patch by patch
            mask = predict_large_image(net, img, device, args.patch_size, args.scale, args.mask_threshold)

        # Save the predicted mask
        filename = os.path.basename(input_file)
        output_file = os.path.join(output_dir, 'M' + f'{os.path.splitext(filename)[0]}.png')
        mask_image = Image.fromarray(mask.astype(np.uint8) * 255)
        mask_image.save(output_file)

        logging.info(f'Mask saved: {output_file}')

        if args.viz:
            # Visualize the image and the mask (optional)
            plot_img_and_mask(img, mask)
