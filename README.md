# UnetTraining_Temple


Ce projet de Deep Learning est développé pour Archeovision dans le cadre de l'apprentissage supervisé de jeux de données. Son objectif est de détecter la présence de pigment bleu sur les murs du Temple d'Apollon à Delphes.

Ce projet est également réalisé dans le cadre de l'UE Projet de Programmation pour la première année du Master Informatique de l'Université de Bordeaux.

## Installation

Ce projet est développé en Python sous la version 3.11.2. Il est recommandé d’utiliser cette version pour garantir son bon fonctionnement. Python 3.11 est disponible [ici](https://www.python.org/downloads/release/python-3112/).

### Création de l'environnement virtuel

1. Créez un environnement virtuel Python :
   ```bash
   python -m venv <nom_de_l'environnement>
   ```

2. Activez l’environnement virtuel :
   - Sous Unix/Linux :
     ```bash
     source <nom_de_l'environnement>/bin/activate
     ```
   - Sous Windows :
     ```bash
     <nom_de_l'environnement>\Scripts\activate
     ```

### Installation des dépendances

1. Placez-vous dans le dossier `unettraining_temple`.

2. Installez les librairies nécessaires en exécutant les commandes :
   ```bash
   pip install -r requirementsTorch.txt
   pip install -r requirements.txt
   ```

   Assurez-vous de respecter l'ordre des commandes pour une installation correcte des librairies.

   **Note :** PyTorch version 1.13 ou supérieure est requise.

## Utilisation

### Générateur du jeu de données

Pour générer le jeu de données d'entraînement :

```bash
python dataset_generator.py -i media -o output_dataset -p 80 -b -e
```

Consultez les options disponibles pour personnaliser la génération du jeu de données.

### Augmentation du jeu de données

Pour augmenter le jeu de données :

```bash
python augment_data.py -i /chemin/vers/dataset -p 0.5 -s _aug -hp 0.7
```

Consultez les options disponibles pour personnaliser l'augmentation des données.

### Création de rapport

Pour générer un rapport de matrice de confusion et d'évaluation des masques prédits :

```bash
python confusion.py -i images -imt maskstrue -imp maskspred -o output -fns -fps
```

Consultez les arguments requis et optionnels pour personnaliser la création du rapport.

### Entraînement d'un modèle UNET

Pour entraîner un modèle UNet :

```bash
python train.py -i dataset -o output -e 1200 -b 20 --amp
```

Consultez les arguments disponibles pour personnaliser l'entraînement du modèle.

### Prédiction

Pour effectuer une prédiction à partir d'images en entrée :

```bash
python predict.py -m MODEL.pth -i images -o masks --viz -t 0.7 --bilinear
```

Consultez les arguments disponibles pour personnaliser la prédiction.

**Note :** Assurez-vous d'utiliser les bonnes options `--bilinear` et `--classes` en fonction des paramètres utilisés lors de l'entraînement du modèle.

```

Ce document Markdown est une simplification du contenu du Manuel d'utilisation. Pour plus d'informations, veuillez vous référer au manuel d'utilisation complet.