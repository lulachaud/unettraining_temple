from pathlib import Path
import sys
from PIL import Image
import os
import json
import random
import imgaug as ia
from imgaug import augmenters as iaa
import cv2
import shutil
import numpy as np
import argparse
from tqdm import tqdm

def count_images(directory):
    # List of image files
    image_files = os.listdir(directory)
    return len(image_files)

def adjust_heat(image, is_warm):
    # Adjust the heat of the image (warm or cool)
    # is_warm determines if the image should be warm (True) or cool (False)

    # Convert the image to HSV color space
    adjusted_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    if is_warm:
        # Increase the hue to make the image warmer
        adjusted_image[:, :, 0] += np.random.randint(1, 20)  # Increase hue randomly
    else:
        # Decrease the hue to make the image cooler
        adjusted_image[:, :, 0] -= np.random.randint(1, 20)  # Decrease hue randomly

    # Convert the image back to BGR color space
    adjusted_image = cv2.cvtColor(adjusted_image, cv2.COLOR_HSV2BGR)

    return adjusted_image

def augment_dataset(image_directory, mask_directory, perc_null, suffix, adjust_heat_prob):
    if perc_null > 100:
        print("Sum of percentages must be equal to 100")
        return

    seq_flip = iaa.Sequential([
        iaa.Fliplr(0.5),  # Horizontal flip with a probability of 0.5
    ])
    seq_rotate = iaa.Sequential([
        iaa.Rotate(90) if np.random.randint(0, 2) == 0 else iaa.Rotate(-90)  # Rotate by 90 or -90 degrees
    ])
    seq_all = iaa.Sequential([
        iaa.Sequential([seq_flip, seq_rotate])
    ])

    image_files = [f for f in os.listdir(image_directory) if f.endswith('.jpg') or f.endswith('.png')]
    total_files = len(image_files)
    
    with tqdm(total=total_files, desc="Augmenting images") as pbar:
        for filename in image_files:
            random_value = np.random.randint(0, 101)
            if random_value <= perc_null:
                image_path = os.path.join(image_directory, filename)
                mask_path = os.path.join(mask_directory, 'M' + filename)

                image_output_path = os.path.join(image_directory, f'{filename[:-4]}_{suffix}.png')
                mask_output_path = os.path.join(mask_directory, 'M' + f'{filename[:-4]}_{suffix}.png')

                shutil.copyfile(image_path, image_output_path)
                shutil.copyfile(mask_path, mask_output_path)

                image = cv2.imread(image_output_path)
                mask = cv2.imread(mask_output_path, cv2.IMREAD_GRAYSCALE)

                # Adjust the heat of the image
                if np.random.rand() < adjust_heat_prob:
                    image = adjust_heat(image, np.random.rand() < 0.5)

                random_value = np.random.randint(0, 3)

                if random_value == 0:
                    images_aug = seq_flip(images=[image, mask])
                elif random_value == 1:
                    images_aug = seq_rotate(images=[image, mask])
                elif random_value == 2:
                    images_aug = seq_all(images=[image, mask])

                image_aug, mask_aug = images_aug[0], images_aug[1]

                # Binarize the mask
                _, mask_aug = cv2.threshold(mask_aug, 128, 255, cv2.THRESH_BINARY)
                cv2.imwrite(image_output_path, image_aug)
                cv2.imwrite(mask_output_path, mask_aug)
            pbar.update(1)

def main(image_directory, mask_directory, perc_null, suffix, adjust_heat_prob):
    print(count_images(image_directory))
    print("Augmentation")
    augment_dataset(image_directory, mask_directory, perc_null * 100, suffix, adjust_heat_prob)
    print(count_images(image_directory))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Augment a dataset of images and masks")
    parser.add_argument("-i", '--input-dir', type=str, help="Path to the dataset directory. The dataset should have an 'image/' and 'mask/' subdirectory", required=True)
    parser.add_argument("-p", "--perc-null", type=float, help="Percentage of images to augment [0,1]", required=True)
    parser.add_argument("-s", "--suffix", type=str, help="Suffix to add to the augmented images", required=True)
    parser.add_argument("-hp", "--adjust_heat_prob", type=float, help="Probability of adjusting the heat of the images [0,1]", required=True)

    args = parser.parse_args()
    image_directory = os.path.join(args.input_dir, 'image')
    mask_directory = os.path.join(args.input_dir, 'mask')
    main(image_directory, mask_directory, args.perc_null, args.suffix, args.adjust_heat_prob)
