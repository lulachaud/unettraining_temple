import os
import numpy as np
from skimage import measure
from PIL import Image
import argparse
import multiprocessing as mp
import matplotlib.pyplot as plt
import cv2 
from tqdm import tqdm

def load_masks_and_files(directory):
    masks = []
    filenames = []
    for filename in sorted(os.listdir(directory)):  # Sort files by name
        if filename.endswith('.png'):
            mask_path = os.path.join(directory, filename)
            mask = np.array(Image.open(mask_path))
            masks.append(mask)
            filenames.append(filename)
    return masks, filenames

def count_connected_components(image):
    if image is None:
        print("Unable to load the image.")
        return
    # Compute connected components
    num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(image, connectivity=8)
    return num_labels - 1

def create_confusion_matrix(true_masks, pred_masks, mask_filenames, mask_pred_filenames, image_files):
    tp = 0  # True Positives
    fp = 0  # False Positives
    fn = 0  # False Negatives
    fn_masks = []  # List to store filenames of false negative masks
    fn_masks_pred = []
    fn_image = []
    fp_masks = []  # List to store filenames of false positive masks
    fp_masks_pred = []
    fp_image = []

    for true_mask, pred_mask, mask_filename, mask_pred_filename, image_file in zip(true_masks, pred_masks, mask_filenames, mask_pred_filenames, image_files):
        if mask_pred_filename != mask_filename:
            print("Error: mask_pred_filename != mask_filename")
            return
        
        # Count the number of connected components in true and predicted masks
        true_components = count_connected_components(true_mask)
        pred_components = count_connected_components(pred_mask)
        
        # Compare connected components between masks
        if pred_components > true_components:
            fp += pred_components - true_components  # False Positives
            fp_masks.append(os.path.basename(mask_filename))
            fp_masks_pred.append(os.path.basename(mask_pred_filename))
            fp_image.append(os.path.basename(image_file))
        elif pred_components < true_components:
            fn += true_components - pred_components  # False Negatives
            # Add the mask filename to the false negative list
            fn_masks.append(os.path.basename(mask_filename))
            fn_masks_pred.append(os.path.basename(mask_pred_filename))
            fn_image.append(os.path.basename(image_file))
        
        # True Positive: number of correctly predicted regions
        tp += min(true_components, pred_components)
        
    return tp, fp, fn, fn_masks, fn_masks_pred, fn_image, fp_masks, fp_masks_pred, fp_image

def generate_plot(args):
    true_mask_path, pred_mask_path, image_file_path, out_dir, title, dir = args

    true_mask = np.array(Image.open(true_mask_path))
    pred_mask = np.array(Image.open(pred_mask_path))
    image = np.array(Image.open(image_file_path))

    image_filename = os.path.splitext(os.path.basename(image_file_path))[0]
    true_mask_filename = os.path.splitext(os.path.basename(true_mask_path))[0]
    pred_mask_filename = os.path.splitext(os.path.basename(pred_mask_path))[0]

    # Create comparison plot of true and predicted masks with the original image
    plt.figure(figsize=(12, 8))

    plt.subplot(1, 3, 1)
    plt.imshow(image, cmap='gray')
    plt.title('Image')
    plt.xlabel(image_filename)  # Filename below the image

    plt.subplot(1, 3, 2)
    plt.imshow(true_mask, cmap='gray')
    plt.title('True Mask')
    plt.xlabel(true_mask_filename)  # Filename below the true mask

    plt.subplot(1, 3, 3)
    plt.imshow(pred_mask, cmap='gray')
    plt.title('Predicted Mask')
    plt.xlabel(pred_mask_filename)  # Filename below the predicted mask

    # Global title for the plot
    plt.suptitle(title)

    # Save the plot in the 'fn' directory
    plot_filename = f"{true_mask_filename}_comparison.png"
    plot_path = os.path.join(out_dir, dir, plot_filename)
    plt.savefig(plot_path)
    plt.close()

def confusion_matrix_plot(tp, fp, fn):
    fig, ax = plt.subplots(figsize=(7.5, 7.5))
    conf_matrix = np.array([[0, fn], [fp, tp]])
    ax.matshow(conf_matrix, cmap=plt.cm.Blues, alpha=0.3)
    for i in range(2):
        for j in range(2):
            if i != 0 or j != 0:
                ax.text(x=j, y=i, s=conf_matrix[i, j], va='center', ha='center', size='xx-large')
    ax.set_xlabel('Predicted', fontsize=18)
    ax.set_ylabel('Expected', fontsize=18)
    ax.set_title('Confusion Matrix', fontsize=18)
    return fig

def evaluate_predict(image_dir, mask_true_dir, mask_pred_dir, out_dir, fn_save, fp_save):
    true_masks, true_mask_files = load_masks_and_files(mask_true_dir)
    pred_masks, pred_mask_files = load_masks_and_files(mask_pred_dir)
    _, image_files = load_masks_and_files(image_dir)
    
    # Compute the confusion matrix to get false negative information
    confusion_matrix_info = create_confusion_matrix(true_masks, pred_masks, true_mask_files, pred_mask_files, image_files)

    fn_masks, fn_masks_pred, fn_image = confusion_matrix_info[3], confusion_matrix_info[4], confusion_matrix_info[5]
    fp_masks, fp_masks_pred, fp_image = confusion_matrix_info[6], confusion_matrix_info[7], confusion_matrix_info[8]
    print("TP:", confusion_matrix_info[0], "FP:", confusion_matrix_info[1], "FN:", confusion_matrix_info[2])
    fig = confusion_matrix_plot(confusion_matrix_info[0], confusion_matrix_info[1], confusion_matrix_info[2])
    
    directory = os.path.join(out_dir)
    if not os.path.exists(directory):
        os.makedirs(directory)

    plot_filename = "confusion_matrix.png"
    plot_path = os.path.join(out_dir, plot_filename)
    fig.savefig(plot_path)
    plt.close(fig)
    print("The Confusion Matrix Has Been Saved")

    tp, fp, fn = confusion_matrix_info[0], confusion_matrix_info[1], confusion_matrix_info[2]
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    report_filename = "report.png"
    report_path = os.path.join(out_dir, report_filename)

    fig, ax = plt.subplots()
    ax.axis('off')
    ax.text(0.5, 0.5, f"Precision: {precision:.4f}\nRecall: {recall:.4f}", ha='center', va='center', fontsize=24)
    fig.savefig(report_path)
    plt.close(fig)

    print(f"Report saved as '{report_path}'.")
    
    if fn_save:
        fn_directory = os.path.join(out_dir, 'fn')
        if not os.path.exists(fn_directory):
            os.makedirs(fn_directory)

        # Use multiprocessing to generate plots in parallel
        pool = mp.Pool(processes=1) 

        # Create a list of arguments for each plot to generate
        args_list = []
        for fn_mask, fn_mask_pred, fn_img in zip(fn_masks, fn_masks_pred, fn_image):
            true_mask_path = os.path.join(mask_true_dir, fn_mask)
            pred_mask_path = os.path.join(mask_pred_dir, fn_mask_pred)
            image_file_path = os.path.join(image_dir, fn_img)
            args_list.append((true_mask_path, pred_mask_path, image_file_path, out_dir, 'Mask Comparison (False Negative)', 'fn'))

        # Generate plots in parallel with a progress bar
        with tqdm(total=len(fn_masks)) as pbar:
            for _ in pool.imap_unordered(generate_plot, args_list):
                pbar.update(1)

        pool.close()
        pool.join()

        print(f"Plots of false negative masks saved in the directory '{fn_directory}'.")

    if fp_save:
        fp_directory = os.path.join(out_dir, 'fp')
        if not os.path.exists(fp_directory):
            os.makedirs(fp_directory)

        # Use multiprocessing to generate plots in parallel
        pool = mp.Pool(processes=1)

        # Create a list of arguments for each plot to generate
        args_list = []
        for fp_mask, fp_mask_pred, fp_img in zip(fp_masks, fp_masks_pred, fp_image):
            true_mask_path = os.path.join(mask_true_dir, fp_mask)
            pred_mask_path = os.path.join(mask_pred_dir, fp_mask_pred)
            image_file_path = os.path.join(image_dir, fp_img)
            args_list.append((true_mask_path, pred_mask_path, image_file_path, out_dir, 'Mask Comparison (False Positive)', 'fp'))

        # Generate plots in parallel with a progress bar
        with tqdm(total=len(fp_masks)) as pbar:
            for _ in pool.imap_unordered(generate_plot, args_list):
                pbar.update(1)

        pool.close()
        pool.join()

        print(f"Plots of false positive masks saved in the directory '{fp_directory}'.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Save Confusion Matrix and Evaluation of Predicted Masks.')
    parser.add_argument('--image_dir', '-i', type=str, required=True, help='Directory containing input images.')
    parser.add_argument('--mask_true_dir', '-imt', type=str, required=True, help='Directory containing ground truth masks.')
    parser.add_argument('--mask_pred_dir', '-imp', type=str, required=True, help='Directory containing predicted masks.')
    parser.add_argument('--out_dir', '-o', type=str, default='output', help='Output directory for saving results.')
    parser.add_argument('--fn_save', '-fns', default=False, action='store_true', help='Save Comparison False Negative Mask Report.')
    parser.add_argument('--fp_save', '-fps', default=False, action='store_true', help='Save Comparison False Positive Mask Report.')
    args = parser.parse_args()
    evaluate_predict(args.image_dir, args.mask_true_dir, args.mask_pred_dir, args.out_dir, args.fn_save, args.fp_save)
