import cv2
import os
import numpy as np
from pathlib import Path
import sys
from PIL import Image
import json
import random
import argparse
from tqdm import tqdm
from multiprocessing import Pool, cpu_count

BACKGROUND_MASK = 0
UNARY_MASK = 255

def count_and_delete_black_masks(image_dir, mask_dir, percentage):
    image_files = os.listdir(image_dir)
    black_mask_count = 0
    image_list = []
    mask_list = []
    white_mask_count = 0

    for image_file in tqdm(image_files):
        image_name, extension = os.path.splitext(image_file)
        if extension.lower() == '.png':
            image_path = os.path.join(image_dir, image_file)
            mask_path = os.path.join(mask_dir, "M" + image_name + ".png")
           
            mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

            if cv2.countNonZero(mask) == 0:
                image_list.append(image_path)
                mask_list.append(mask_path)
            else:
                white_mask_count += 1
    
    if percentage == 100:
        return
    else:
        max_size = white_mask_count * 100 / (100 - percentage)

    num_elements_to_keep = int(max_size * percentage * 0.01)
    print("Number of black masks to keep:", num_elements_to_keep)
    
    elements_to_keep = random.sample(range(len(image_list)), num_elements_to_keep)
    
    for i in range(len(image_list)):
        if i not in elements_to_keep:
            black_mask_count += 1
            import send2trash
            send2trash.send2trash(image_list[i])
            send2trash.send2trash(mask_list[i])

def isIn(x, y, w, h):
    return x >= 0 and y >= 0 and x < w and y < h

def spreadColor(img, ix, iy):
    w, h = img.size
    color = img.getpixel((ix, iy))
    marked = [(ix, iy)]
    while marked:
        x, y = marked.pop(0)
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                nx = x + dx
                ny = y + dy
                if isIn(nx, ny, w, h):
                    if img.getpixel((nx, ny)) == UNARY_MASK:
                        marked.append((nx, ny))
                        img.putpixel((nx, ny), color)

def createMask(full_img, image_name, json_path):
    width, height = full_img.size
    
    with open(json_path, 'r', encoding="utf-8") as json_file:
        data = json.load(json_file)
    
    pixels = []
    for d in data:
        if "RGB" in d:
            for area in data[d]:
                pixels.append(json.JSONDecoder().decode(data[d][area]))

    mask = Image.new('L', full_img.size, BACKGROUND_MASK)

    found_pos = False
    for pix in pixels:
        for p in pix:
            mask.putpixel(p, UNARY_MASK)
            found_pos = True

    return mask, found_pos

def saveMasks(img, mask, sub_img_size, bin, image_name, dest_dir, choose_number, black_masks): 
    pp = 100
    cropped_images = cropImages(img, sub_img_size)
    cropped_masks = cropImages(mask, sub_img_size)

    num_images = len(cropped_images)

    positive_index = []
    for i in range(num_images):
        if sum(cropped_masks[i].getextrema()) != 0: 
            positive_index.append(i)
        else:
            if choose_number:
                if black_masks > 0:
                    positive_index.append(i)
                    black_masks -= 1
            else:
                positive_index.append(i)
            
    negative_index = list(set(range(num_images)).difference(set(positive_index)))

    num_pos = len(positive_index)
    num_neg = len(negative_index)

    target_num_neg = int((num_pos * (100 - pp)) / pp)

    save_index = []
    save_index.extend(positive_index)
    
    if target_num_neg < num_neg:
        if pp != 100:
            save_index.extend(random.sample(negative_index, target_num_neg))
    else:
        save_index.extend(negative_index)
    
    for i in save_index:
        cropped_images[i].save(f"{dest_dir}/image/{image_name}_{i}.png")
        if i in positive_index:
            if i in positive_index and not bin:
                color_i = 1
                for xp in range(sub_img_size):
                    for yp in range(sub_img_size):
                        if cropped_masks[i].getpixel((xp, yp)) == UNARY_MASK:
                            cropped_masks[i].putpixel((xp, yp), color_i)
                            spreadColor(cropped_masks[i], xp, yp)
                            color_i += 1
            cropped_masks[i].save(f"{dest_dir}/mask/M{image_name}_{i}.png")

def cropImages(full_img, sub_img_size):
    cropped_img = []
    x = 0
    
    width, height = full_img.size
    while x < width:
        y = 0
        if x + sub_img_size >= width:
            x = width - sub_img_size
        while y < height:
            if y + sub_img_size >= height:
                y = height - sub_img_size
            cropped_img.append(full_img.crop((x, y, x + sub_img_size, y + sub_img_size)))
            y += sub_img_size
        x += sub_img_size
    return cropped_img

def process_image(args):
    f, src_dir, dest_dir, sub_img_size, bin, choose_number, black_masks = args
    full_img = Image.open(f)
    image_name = Path(f).stem
    json_path = os.path.join(src_dir, "jsons", image_name + ".json")
    if not os.path.isfile(json_path):
        return image_name, False
    mask, found_pos = createMask(full_img, image_name, json_path)
    if found_pos:
        saveMasks(full_img, mask, sub_img_size, bin, image_name, dest_dir, choose_number, black_masks)
    return image_name, found_pos

def generateDataset(sub_img_size, src, dest, bin, choose_number, black_masks, percentage, erode, pp):
    src_dir = src
    dest_dir = dest
    skipped_images = []
    if not os.path.exists(dest_dir):
        os.mkdir(dest_dir)
    if not os.path.exists(dest_dir + "/image/"):
        os.mkdir(dest_dir + "/image/")
    if not os.path.exists(dest_dir + "/mask/"):
        os.mkdir(dest_dir + "/mask/")

    image_extensions = ('*.jpg', '*.jpeg', '*.JPG', '*.png')
    src_files = []
    for ext in image_extensions:
        src_files.extend(Path(src_dir + os.sep + "images").glob(ext))

    total_images = len(src_files)
    
    pool_args = [(f, src_dir, dest_dir, sub_img_size, bin, choose_number, black_masks) for f in src_files]
    with Pool(cpu_count()) as pool:
        results = list(tqdm(pool.imap(process_image, pool_args), total=total_images))

    for image_name, found_pos in results:
        if not found_pos:
            skipped_images.append(image_name)

    if not choose_number:
        count_and_delete_black_masks(dest_dir + "/image", dest_dir + "/mask", percentage=percentage)
    if erode:
        erode_images(dest_dir + "/mask")
    if pp != 100:
        positive_masks = [f for f in os.listdir(dest_dir + "/mask/") if not cv2.countNonZero(cv2.imread(os.path.join(dest_dir + "/mask/", f), cv2.IMREAD_GRAYSCALE)) == 0]
        print("Number of positive masks:", len(positive_masks))
        num_elements_to_keep = int(len(positive_masks) * ((100 - pp) / 100))
        print("Number of positive masks to keep:", num_elements_to_keep)
        elements_to_keep = random.sample(range(len(positive_masks)), num_elements_to_keep)
        for mask in positive_masks:
            if mask not in elements_to_keep:
                os.remove(os.path.join(dest_dir + "/mask/", mask))
                os.remove(os.path.join(dest_dir + "/image/", mask.replace("M", "")))
    if skipped_images:
        print("\nThe following images were skipped as they do not have a ground truth JSON file:")
        for image in skipped_images:
            print(image)

def erode_images(input_folder):
    for filename in os.listdir(input_folder):
        if filename.endswith((".png", ".jpg", ".jpeg")):
            input_path = os.path.join(input_folder, filename)

            img = cv2.imread(input_path, 0)
            if img is not None:
                kernel = np.ones((3, 3), np.uint8)
                img_erosion = cv2.erode(img, kernel, iterations=1)

                cv2.imwrite(input_path, img_erosion)

def main():
    parser = argparse.ArgumentParser(description='Generate a dataset')
    parser.add_argument('-npm', '--number-per-mask', type=int, help='Number of black masks to keep per image')
    parser.add_argument('-pp', '--positive-percentage', type=int, help='Percentage of positive masks to keep in the sample of positives masks, value must be between 0 and 100 inclusively', default=100)
    parser.add_argument('-p', '--percentage', type=int, help='Percentage of black masks to keep, value must be between 0 and 100 inclusively')
    parser.add_argument('-i', '--input-dir', type=Path, help='Path to the directory containing \'images\\\' and \'jsons\\\' subfolders', required=True)
    parser.add_argument('-o', '--output-dir', type=Path, help='Path to the directory where the new dataset will be written', required=True)
    parser.add_argument('-s', '--sub-image-size', type=int, help='Size of the images to be created for the dataset (Default 256)', required=False, default=256)
    parser.add_argument('-b', '--binary', help='If set, mask will be binary (black/white), else each instance will be encoded with a different shade of grey', default=False, action='store_true')
    parser.add_argument('-e', '--erode', help='Erode Image', default=False, action='store_true')
    args = parser.parse_args()

    choose_number = False
    black_masks = 0
    percentage = 0

    if args.number_per_mask:
        if args.percentage:
            print("Error: Cannot use both -npm and -p options")
            sys.exit(1)
        choose_number = True
        black_masks = args.number_per_mask
    elif args.percentage:
        percentage = args.percentage
        if percentage < 0 or percentage > 100:
            raise ValueError("Error: The percentage must be between 0 and 100 inclusively.")
    abs_indir = args.input_dir.resolve()
    abs_odir = args.output_dir.resolve()
    generateDataset(args.sub_image_size, str(abs_indir), str(abs_odir), args.binary, choose_number, black_masks, percentage, args.erode, args.positive_percentage)

if __name__ == "__main__":
    main()
